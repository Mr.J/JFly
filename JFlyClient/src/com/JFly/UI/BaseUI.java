package com.JFly.UI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import com.JFly.Tools.SocketHelper;

public class BaseUI {
	public void init() throws UnknownHostException, IOException{
		Socket s = new Socket("127.0.0.1", 8080);
		
		JFrame jf = new JFrame("JFly");
		
		JButton jb_send = new JButton("send");
		
		JTextArea jta_head = new JTextArea();
		JTextArea jta_input = new JTextArea();
		
		JPanel jp_head = new JPanel();
		JPanel jp_foot = new JPanel();
		JPanel jp_space = new JPanel();
		//jp_head.setBackground(Color.ORANGE);
		jb_send.setSize(100, 345);
		jta_head.setRows(30);
		jta_head.setColumns(50);
		jta_head.setAutoscrolls(true); 
		
		jta_input.setRows(5);
		jta_input.setColumns(44);
		jta_input.setAutoscrolls(true); 
		
		jp_head.add(jta_head,BorderLayout.CENTER);
		
		jp_foot.setLayout(new BorderLayout());
//		jp_foot.setSize(800, 400);
//		jp_foot.setBackground(Color.blue);
		jp_foot.add(jb_send,BorderLayout.EAST);
		jp_foot.add(jta_input,BorderLayout.WEST);
		jp_foot.add(jp_space,BorderLayout.CENTER);
		
		
		jb_send.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				try {
					String Text = jta_input.getText();
					System.out.println("A Text : " + Text);
					SocketHelper.sendString(s, Text);
					jta_input.setText("");
				} catch (UnknownHostException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		
		jf.setVisible(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		jf.getContentPane().setLayout(new FlowLayout());
		jf.setSize(1024, 700);
		jf.getContentPane().add(jp_head,BorderLayout.NORTH);
		jf.getContentPane().add(jp_foot,BorderLayout.SOUTH);
	}
}
