package com.JFly.msg;

import java.io.IOException;
import java.net.Socket;

import com.JFly.Tools.SocketHelper;

public class TextMessage extends Message{
	protected String Text;
	public TextMessage(MessageConnector sender, MessageConnector receiver, String Text) {
		super(sender, receiver);
		// TODO Auto-generated constructor stub
		this.Text = Text;
	}
	
	public void setText(String Text){
		this.Text = Text;
	}
	
	public String getText(){
		return this.Text;
	}
	
	public void process() {
		Socket s = this.receiver.socket;
		if(s == null) throw new NullPointerException("receiver socket is Null");
		else{
			try {
				System.out.println(SocketHelper.readString(receiver.socket));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
