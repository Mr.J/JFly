package com.JFly.msg;

public abstract class Message {
	//消息的发送者
	MessageConnector sender;
	//消息的接受者
	MessageConnector receiver;
	//处理消息的函数
	public void process() {}
	
	public Message(MessageConnector sender, MessageConnector receiver) {
		this.sender = sender;
		this.receiver = receiver;
	}
}
